# Euclidean minimum spanning tree
class EMST:
    def __init__(self):
        self.nodes = []
        self.min_route = None

class Edge:
    def __init__(self, value, node):
        self.value = value
        self.node = node

class Node:
    # float -> float -> bool -> Node -> [Edge] -> Node
    # Long = x , Lat = y
    def __init__(self, lat, long_, visited=False, next_node=None):
        self.lat = lat 
        self.long = long_
        self.visited = visited
        self.next_node = next_node
        self.edges = []

def run(): 
    pass

