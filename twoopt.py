#!/usr/bin/python3
from scipy.spatial import distance_matrix
import util as u
import matplotlib.pyplot as plt
import numpy as np


def fit(nodes):
    x,y = get_cords(nodes)
    z = list(zip(x,y))
    return distance_matrix(z,z)


def get_cords(data):
    x = []
    y = []
    for c in data:
        x.append(c[1])
        y.append(c[0])
    return x, y


def two_opt_fast(d_mat, nodes):
    count = 0
    size = len(nodes) - 1
    nodes = nodes.tolist()
    print(nodes)
    print(size)
    while True:
        minchange = 0 
        for i in range(0, size - 2):
            new_route = nodes[:]
            minI = None
            for j in range(i + 2, size):
                change = d_mat[i,j] + d_mat[i+1, j+1] - d_mat[i, i+1] - d_mat[j, j+1]
                if minchange > change: 
                    minchange = change
                    minI = i
                    minJ = j
                    new_route[i:j] = nodes[minJ-1:minI-1:-1]
            if minI:
                nodes = new_route[:]
            count += 1
            if count % 5000 == 0:
                print(count)
        if minchange <= 0 or count > 50000:
            break

    return nodes


def two_opt_cords(nodes):
    x = []
    y = []
    for cords in nodes:
        x.append(cords[1])
        y.append(cords[0])
    return x, y


def plot_nodes(x, y):
    plt.scatter(x, y, zorder=2)
    plt.plot(x, y, 'r', zorder=1)
    plt.show()


if __name__ == "__main__":
    data = u.read_csv_no_header(
        "C:\\Users\\UPsafety Admin\\Documents\\prototypes\\route_op\\greenville_clean_data.csv")
    data = u.read_csv_no_header("./route_op/test.csv")

    nodes = data
    d_mat = fit(nodes)
    two_opt_nodes = two_opt_fast(d_mat, nodes)
    x,y = two_opt_cords(two_opt_nodes)
    print(two_opt_nodes)
    plot_nodes(x,y)