import util as u
from scipy.spatial import distance_matrix
from disjoint_set import *
from edge import *
import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
import time

# Source Wikipedia: https://en.wikipedia.org/wiki/Kruskal%27s_algorithm#Pseudocode
# KRUSKAL(G):
# A = ∅
# foreach v ∈ G.V:
#    MAKE-SET(v)
# foreach (u, v) in G.E ordered by weight(u, v), increasing:
#    if FIND-SET(u) ≠ FIND-SET(v):
    #   A = A ∪ {(u, v)}
    #   UNION(u, v)
# return A

def kruskal(edges, verts):
    A = disjoint_set()
    for v in range(verts): 
        A.make_set(v)
    for edge in edges:
        if A.find_com(edge.vert[0]) != A.find_com(edge.vert[1]): 
            A.union_s(edge.vert[0], edge.vert[1])
    return A 


def get_edges(data):
    edges = defaultdict(None) 
    dist_mat = distance_matrix(data,data)
    for x, row in enumerate(dist_mat):
        for y, dist in enumerate(row): 
            ## Skip dieangle in distance matrix, that represents an edge to itself. 
            if y is x: continue
            tmp = edge(dist, (x,y))
            vert = tuple(sorted((x,y)))
            if vert not in edges: 
                edges[vert] = tmp
    return edges

def rank_edges(edges_dict): 
    return sorted(list(edges_dict.values()), key=lambda x : x.length) 

def get_cords_from_data(data):
    x = data[:, 0]
    y = data[:, 1]
    return x, y


def get_cords_from_dj_set(dj,data):
    cords = np.ndarray(shape=data.shape,buffer=data[dj.sets[0]])
    # for i in dj.sets[0]:
    #     cords.append(data[i, :])
    x , y = get_cords_from_data(cords)

    return x, y


def plot_nodes(x, y):
    plt.scatter(x, y, zorder=2)
    plt.plot(x, y, 'r', zorder=1)
    plt.show()

def orginal_path(file):
    data = u.read_csv_no_header(file)
    x, y = get_cords_from_data(data)
    plot_nodes(x,y)     

def kruskal_path(file):
    data = u.read_csv_no_header(file)

    edges = get_edges(data)
    edges = rank_edges(edges)
    num_verts = data.shape[0]
    mst = kruskal(edges, num_verts)
    x,y = get_cords_from_dj_set(mst, data)
    plot_nodes(x, y)

def main(file="C:\\Users\\UPsafety Admin\\Documents\\prototypes\\route_op\\greenville_clean_data.csv"):
    # data = u.read_csv_no_header("./route_op/test.csv")
    data = u.read_csv_no_header(file)

    edges = get_edges(data)
    edges = rank_edges(edges)
    num_verts = data.shape[0]
    mst = kruskal(edges, num_verts)

    x,y = get_cords_from_dj_set(mst, data)
    plot_nodes(x, y)

if __name__ == '__main__':
 
    main()