# -*- coding: utf-8 -*-

"""
Created on Fri Sep 21 09:47:12 2018

@author: UPsafety Admin
"""

import util as u
import matplotlib.pyplot as plt
import random
from math import sqrt
import time
#import googlemaps




class Node:
    # float -> float -> bool -> Node -> [Edge] -> Node
    # Long = x , Lat = y
    def __init__(self, x, y, visited=False, next_node=None, next_node_dist = None):
        self.x = x
        self.y = y
        self.visited = visited
        self.next_node = next_node
        self.next_node_dist = next_node_dist

    # Returns the cords in a csv format 
    def toString(self):
        return str(self.y) + "," + str(self.x)

def main():
    data = u.read_csv_no_header(
        "C:\\Users\\UPsafety Admin\\Documents\\prototypes\\route_op\\greenville_clean_data.csv")
    # sys.setrecursionlimit(20000)
    nodes = create_nodes(data)
    #write_nodes_to_file("./orginal.csv", nodes )
    x,y = get_cords_from_nodes_ordered(nodes)
    plot_nodes(x,y)
    
    nodes, starting_loc = start_nn(nodes)
    #write_nodes_to_file("NN_op.csv", order_nodes(nodes, starting_loc))
    
    o_nodes = order_nodes(nodes, starting_loc)
    start_two_opt(o_nodes)

def write_nodes_to_file(file, nodes):
    f = open(file, "w")
    for node in nodes:
        f.write(node.toString() + "\n")

def start_two_opt(nodes):
    two_opt_nodes = two_opt(nodes)
    two_x, two_y = get_cords_from_nodes_ordered(two_opt_nodes)

    write_nodes_to_file("./twoopt.csv", two_opt_nodes)
    plot_nodes(two_x, two_y)


def start_nn(nodes):
    time.process_time()
    nn_nodes, starting_loc = nn(nodes)
    end = time.process_time()
    print("end time -> NN", end)
    x, y = get_cords_from_nodes(nn_nodes, starting_loc)
    plot_nodes(x, y)

    return nn_nodes, starting_loc

# Given Starting node, return distance of tour
# Node -> int
def find_tour_dist(node):
    sum_dist = 0
    while node is not None:
        sum_dist += node.next_node_dist
        node = node.next_node
    return sum

def get_cords_from_nodes_ordered(nodes):
    x = []
    y = []
    for node in nodes:
        x.append(node.x)
        y.append(node.y)
    return x, y


def get_cords_from_nodes(nodes, start_loc):
    node = nodes[start_loc]
    x = []
    y = []
    while node is not None:
        x.append(node.x)
        y.append(node.y)
        node = node.next_node
    return x, y


def plot_nodes(x, y):
    plt.scatter(x, y, zorder=2)
    plt.plot(x, y, 'r', zorder=1)
    plt.show()


# array-like -> [Nodes]
# Long = x , Lat = y
def create_nodes(mat):
    return [Node(row[1], row[0]) for row in mat]


# [Nodes] -> [Nodes]
# Nearest neighbor solutuion.
# Returns: starting node, and the path
# Pro: Fast
# Con: Could be a bad path, no gaureentee of a good distance
def nn(nodes):

    #gmaps = googlemaps.Client(key="AIzaSyDyzppdJbcqK0BzPhcxvnVDBemn7WyMFXg")

    count = 0
    starting_loc = random.randint(0, len(nodes)-1)

    nodes[starting_loc].visited = True
    curr = nodes[starting_loc]
    curr_loc = starting_loc

    while True:

        n_neighbor, dist = find_closest_node(curr, nodes)

        if n_neighbor is None:
            return nodes, starting_loc

        # Set curr node values
        nodes[n_neighbor].visited = True
        nodes[curr_loc].next_node = nodes[n_neighbor]
        nodes[curr_loc].next_node_dist = dist

        # Update curr node
        curr_loc = n_neighbor
        curr = nodes[n_neighbor]

        # Count nodes for debuging 
        count += 1
        if count % 200 is 0:
            print("On node ", count)


def find_closest_node(curr, nodes):
    min_dist = 9999999
    min_node = None
    min_e_dist = 999999
    for e, node in enumerate(nodes):
        if not node.visited:
            # Trying to cut down on gmap calls to increase speed
            dist = euclidean_dist(curr, node)
            # TODO This should be changed to incorpate a larger scope
            if dist < min_e_dist:
                min_e_dist = dist
                # dist = get_gmap_dist(curr, node, gmaps)
                if dist < min_dist:
                    min_dist = dist
                    min_node = e
    return min_node, min_dist


# Node -> Node -> gmaps -> int
# Find the walking distance from 2 points
# Limited to 50 calls per second
def get_gmap_dist(start, dest, gmaps):
    start_string = str(start.y) + "," + str(start.x)
    dest_string = str(dest.y) + "," + str(dest.x)
    directions_result = gmaps.directions(start_string,
                                         dest_string,
                                         mode="walking")
    return int(directions_result[0]['legs'][0]['distance']['value'])


def euclidean_dist(p, q):
    return sqrt((p.x - q.x) ** 2 + (p.y - q.y) ** 2)


# repeat until no improvement is made {
#        start_again:
#        best_distance = calculateTotalDistance(existing_route)
#        for (i = 1; i < number of nodes eligible to be swapped - 1; i++) {
#            for (k = i + 1; k < number of nodes eligible to be swapped; k++) {
#                new_route = 2optSwap(existing_route, i, k)
#                new_distance = calculateTotalDistance(new_route)
#                if (new_distance < best_distance) {
#                    existing_route = new_route
#                    goto start_again
#                }
#            }
#        }
#    }


## Might not actually be needed. 
def order_nodes(nodes, starting_loc): 
    node = nodes[starting_loc]
    ordered_nodes = []
    while node is not None: 
        ordered_nodes.append(node)
        node = node.next_node
    return ordered_nodes

# Takes in a set nodes ordered by route
# Returns the cost to tranverse route
def find_tour_dist_ordered(nodes):
    dist_sum = 0
    for i in range(0, len(nodes) - 1):
        for j in range(i + 1, len(nodes)):
            dist_sum = euclidean_dist(nodes[i], nodes[j])
    return dist_sum 
        

        

def two_opt(nodes):
    count = 0 
    count2 = 0
    route = nodes
    improved = True
    best_route = nodes
    MAX_IMPROVEMENTS = 50
    while improved and count < MAX_IMPROVEMENTS:
        best_distance = find_tour_dist_ordered([best_route]) 
        improved = False
        for i in range(len(nodes) - 2):
            for j in range(i + 1, len(nodes)):
                if j - i == 1: continue
                new_route = nodes[:] 
                new_route[i:j] = route[j-1:i-1:-1] # this is the 2woptSwap
                new_dist = find_tour_dist_ordered(new_route)
                count2 += 1
                print(count2)
                if count is MAX_IMPROVEMENTS:
                    break
                if new_dist < best_distance: 
                    best_distance = new_dist 
                    best_route = new_route
                    improved = True
                    count +=1
                    break
            print(count, " Improvements made")
            if count is MAX_IMPROVEMENTS: 
                break
    return best_route


# def two_opt(route):
#      best = route
#      improved = True
#      while improved:
#           improved = False
#           for i in range(1, len(route)-2):
#                for j in range(i+1, len(route)):
#                     if j-i == 1: continue # changes nothing, skip then
#                     new_route = route[:]
#                     new_route[i:j] = route[j-1:i-1:-1] # this is the 2woptSwap
#                     if cost(new_route) < cost(best):
#                          best = new_route
#                          improved = True
#           route = best
#      return best


#   2optSwap(route, i, k) {
#        1. take route[0] to route[i-1] and add them in order to new_route
#        2. take route[i] to route[k] and add them in reverse order to new_route
#        3. take route[k+1] to end and add them in order to new_route
#        return new_route;
#    }


if __name__ == "__main__":
    main()
