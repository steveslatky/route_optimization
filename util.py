#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import math

def read_csv(path):
    return np.genfromtxt(path, delimiter=',',skip_header=2)

def read_csv_no_header(path):
    return np.genfromtxt(path, delimiter=',',skip_header=False)


def standardize(matrix):
    if len(matrix) > 1 or len(matrix) == 0:
        # return tuple([_std_feature(f) for f in matrix])
        return np.array([_std_feature(f) for f in matrix])
    else:
        return _std_feature(matrix[0])


def _std_feature(feature):
    _mat = feature.transpose()
    std = np.std(_mat, axis=0, ddof=1)
    if np.any(std == 0.0):
        return _mat
    # print("Std:" + str(std))
    return (_mat - np.mean(_mat, axis=0)) / np.std(_mat, axis=0, ddof=1)


def std(mat):
    return np.array([_std_f(f) for f in mat.T])

def _std_f(f):
    std = np.std(f, ddof=1)
    print("Std: " + str(std))
    return (f - np.mean(f) / std)


def single_matrix_plot(matrix):
    plt.plot(matrix, 'ro')
    plt.show()


def e_distance(p,q):
    return math.sqrt((p[0] - q[1]) ** 2 + (p[1] - q[1]) ** 2)

def std_n_l(mat):
    l_col = mat[:, mat.shape[1] - 1:]
    f_col = mat[:, :mat.shape[1] - 1]
    std_m = standardize(f_col.T)

    std_m = np.append(std_m, l_col)
    std_m = std_m.reshape(mat.shape)

    return std_m

def accuracy(y_pred, y_true):
    if y_pred.shape != y_true.shape:
        raise ValueError('Predicted and actual targets are of different shape')

    return np.sum((y_pred == y_true)) / y_pred.shape[0]


def precision(y_pred, y_true, positive_class=1.0, negative_class=0.0):
    """True positives over true and false positives."""
    true_positives = np.sum((y_pred == positive_class) & (y_true == positive_class))
    false_positives = np.sum((y_pred == positive_class) & (y_true == negative_class))

    return true_positives / (true_positives + false_positives)

        
def recall(y_pred, y_true, positive_class=1.0, negative_class=0.0):
    """True positives over true positives and false negatives."""
    true_positives = np.sum((y_pred == positive_class) & (y_true == positive_class))
    false_negatives = np.sum((y_pred == negative_class) & (y_true == positive_class))

    return true_positives / (true_positives + false_negatives)

def f_measure(y_pred, y_true, positive_class=1.0, negative_class=0.0):
    """2 * precision * recall / (precision + recall)."""
    precision_val = precision(y_pred, y_true, positive_class, negative_class)
    recall_val = recall(y_pred, y_true, positive_class, negative_class)

    return 2 * precision_val * recall_val / (precision_val + recall_val)