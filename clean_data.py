import numpy as np
import util as u 

def main():
    data = u.read_csv('./greenville-locations-from-2017-12-28.csv')
    # Get GPS data cols
    data = data[:, 2:]

    # Find and delete columns with no data 
    rm_list = []
    for e,i in enumerate(data):
        if (np.isnan(i)).any():
            rm_list.append(e)
    data = np.delete(data, rm_list, axis=0)

    np.savetxt("./data_cleaned.csv", data, delimiter=",")

if __name__ == "__main__":
    main() 