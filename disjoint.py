# Author Stephen Slatky
# Date: 2018-10-03
# Version: 0.1 -> inital setup 

# Source: Wikipedia -> https://en.wikipedia.org/wiki/Disjoint-set_data_structure

# The parent pointers of elements are arranged to form one or more trees, each representing a set. 
# If an element's parent pointer points to no other element, then the element is the root of a tree
#  and is the representative member of its set. A set may consist of only a single element. However, 
# if the element has a parent, the element is part of whatever set is identified by following the 
# chain of parents upwards until a representative element (one without a parent) is reached at 
# the root of the tree.


class DJ_node:
# A disjoint-set forest consists of a number of elements each of which stores an id, a parent 
# pointer, and, in efficient algorithms, either a size or a "rank" value.
    def __init__(self, _id, parent=None, size=None, rank=None ): 
        self.id = _id
        self.parent = parent
        self.size = size
        self.rank = rank

    def append_set(self, new_set):
        pass

    def __eq__(self, nn):
        if self.id is nn.id and self.parent is nn.parent and self.size is nn.size and self.rank is nn.rank:
            return True
        else: return False

class disjoint_set:

# Forests can be represented compactly in memory as arrays in which parents are indicated by their 
# array index. 
    def __init__(self, elem=None): 
        self.sets = [] # Forest

    def make_set(self, elem): 
        pass

    
    # Path compression
    # Path compression flattens the structure of the tree by making every node point to the root 
    # whenever Find is used on it. This is valid, since each element visited on the way to a root 
    # is part of the same set. The resulting flatter tree speeds up future operations not only on 
    # these elements, but also on those referencing them. 
    def find_com(self, elem):
        pass 

    # Union by size
    def union_size(self, elem1, elem2):
        pass



    def find_split(self):
        pass