#!/usr/bin/python3
import util as u
import matplotlib.pyplot as plt
#from math import sqrt
from scipy.spatial import distance
import numpy as np
import time

def two_opt(file):

    data = u.read_csv_no_header(file)
    data_list = data_to_list(data)
    
    new_route = two_opt_algo(data_list)

    plot(new_route)

def plot(route):
    x = [point[0] for point in route] 
    y = [point[1] for point in route] 
    plot_cords(y,x)
    
def plot_cords(x, y):
    plt.scatter(x, y, zorder=2)
    plt.plot(x, y, 'r', zorder=1)
    plt.show()

# Takes in data returns a list
# of cords in side a tuple. 
def data_to_list(data):
    return [tuple(row) for row in data]
    
def two_opt_algo(route):
     best = route
     improved = True
     while improved:
          improved = False
          for i in range(1, len(route)-2):
               for j in range(i+1, len(route)):
                    if j-i == 1: continue # changes nothing, skip then
                    new_route = route[:]
                    new_route[i:j] = route[j-1:i-1:-1] # this is the 2woptSwap
                    if tour_cost(new_route) < tour_cost(best):
                         best = new_route
                         improved = True
          route = best
     return best

# Takes in a set nodes ordered by route
# Returns the cost to tranverse route
def tour_cost(nodes):
    dist_sum = 0
    for i in range(0, len(nodes) - 1):
            dist_sum += distance.euclidean(nodes[i], nodes[i + 1])
    return dist_sum

def euclidean_dsit(a,b):
    a_min_b = a - b
    np.sqrt(np.einsum('ij,ij->i', a_min_b, a_min_b))

#def euclidean_dist(x,y):
#    return np.sqrt(np.sum((x-y)**2))

#def euclidean_dist(p, q):
#    return sqrt((p[0] - q[0]) ** 2 + (p[1] - q[1]) ** 2)


def orginal_path(file):
    data = u.read_csv_no_header(file)
    data_list = data_to_list(data)
    plot(data_list)
    return(data_list)

def test():
    orginal_path("./small_test.csv")
    two_opt("./small_test.csv")



if __name__ == "__main__":
#    two_opt("./writeup/test.csv")
    t0 = time.time()
    two_opt("./test.csv")
    t1 = time.time()
    print("seconds: ", t1-t0)