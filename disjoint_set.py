
# Author Stephen Slatky
# Date: 2018-10-03
# Version: 0.1 -> inital setup 

# Source: Wikipedia -> https://en.wikipedia.org/wiki/Disjoint-set_data_structure

# The parent pointers of elements are arranged to form one or more trees, each representing a set. 
# If an element's parent pointer points to no other element, then the element is the root of a tree
#  and is the representative member of its set. A set may consist of only a single element. However, 
# if the element has a parent, the element is part of whatever set is identified by following the 
# chain of parents upwards until a representative element (one without a parent) is reached at 
# the root of the tree.

# Forests can be represented compactly in memory as arrays in which parents are indicated by their 
# array index. 

class disjoint_set:

# A disjoint-set forest consists of a number of elements each of which stores an id, a parent 
# pointer, and, in efficient algorithms, either a size or a "rank" value.
    def __init__(self, elem=None): 
        self.sets = []
        if elem: 
            self.sets.append([elem]) 

    def make_set(self, elem): 
        self.sets.append([elem])

    # Path compression
    # Path compression flattens the structure of the tree by making every node point to the root 
    # whenever Find is used on it. This is valid, since each element visited on the way to a root 
    # is part of the same set. The resulting flatter tree speeds up future operations not only on 
    # these elements, but also on those referencing them. 
    def find_com(self, elem):
        for _set in self.sets:
            if elem in _set:
                return _set
        raise ValueError("Element was not found in the Disjoint-set") 


    # Union by size
    def union_s(self, elem1, elem2):
        set_1 = self.find_com(elem1)
        set_2 = self.find_com(elem2)

        if set_1 == set_2:
            return
        
        set_1.extend(set_2)
        self.sets.remove(set_2)




    def find_split(self):
        pass